# Acl Airtel Money Gateway Payment

__ACL Airtel Money GA Payment Gateway__ est une extension WooCommerce qui vous permet de supporter les paiements via Airtel Money pour le Gabon.

## Pré-requis

Vous aurez besoin :
- un compte marchand Airtel Money
- un compte développeur sur __https://developers.airtel.ga/__

Après avoir créé votre compte développeur et votre application sur https://developers.airtel.ga/, récupérez clés (client id et client secret). Vous en aurez besoin pour faire fonctionner votre plugin.

## Installation

- Connectez-vous à votre hébergement via FTP (Filezilla par exemple).
- Dans le dossier wp-content/plugins/ créez un dossier __acl-airtel-money-gateway-payment__
- Déposez le fichier le fichier __acl-am-gateway.php__ dans __acl-airtel-money-gateway-payment__

C'est fini !
Il ne vous reste plus qu'à activer votre plugin via le menu Extensions > Extensions installées dans l'administration de votre site.

## Configuration

Rendez-vous dans le menu WooCommerce > Réglages de votre administration WordPress. Dans l'onglet Paiements, la méthode __ACL Airtel Money GA__ apparaît désormais. Cliquez sur le bouton __Configuration__ dans la dernièere colonne du tableau de méthodes.

Dans l'écran de configuration, vous pourrez activer/désativer la méthode de paiement. Il est aussi possible d'activer le plugin en mode __Test__ pour vérifier le fonctionnement du plugin. Remplissez suivant vos besoins, le couple Test Client Id/Test Client Secret ou Live Client Id/Live Client Secret.

Vos clients peuvent désormais réaliser des paiements via Airtel Money GA.

## Contacts

En cas d'erreur, n'hésitez pas à contacter le développeur en envoyant un email à karl@acreativelab.com .
