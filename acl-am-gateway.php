<?php
/*
 * Plugin Name: ACL Airtel Money GA Payment Gateway
 * Plugin URI: https://acreativelab.com/nos-plugins/woocommerce-airtel-money-ga-payment-gateway
 * Description: ACL Airtel Money GA Payment Gateway est une extension qui vous permet de supporter les paiements via Airtel Money pour le Gabon
 * Author: A Creative Lab
 * Author URI: http://acreativelab.com
 * Version: 1.0.2
 */

/*
 * This action hook registers our PHP class as a WooCommerce payment gateway
 */
add_filter('woocommerce_payment_gateways', 'acl_am_ga_add_gateway_class');
function acl_am_ga_add_gateway_class($gateways) {
	$gateways[] = 'WC_AclAMGa_Gateway';
	return $gateways;
}

/*
 * The class itself, please note that it is inside plugins_loaded action hook
 */
add_action('plugins_loaded', 'acl_am_init_gateway_class');
function acl_am_init_gateway_class() {

	class WC_AclAMGa_Gateway extends WC_Payment_Gateway {

        private $base_uri_test = 'https://openapiuat.airtel.africa/';
        private $base_uri = 'https://openapi.airtel.africa/';
        private $payment_uri = 'merchant/v1/payments/';
        private $auth_uri = 'oauth2/token/';

 		/**
 		 * Class constructor, more about it in Step 3
 		 */
        public function __construct() {

            $this->id = 'aclamga';
            $this->icon = ''; // URL of the icon
            $this->has_fields = true;
            $this->method_title = 'ACL Airtel Money GA';
            $this->method_description = 'Accepter les paiements via Airtel Money (Gabon).'; // will be displayed on the options page
        
            // gateways can support subscriptions, refunds, saved payment methods,
            // Simple payments
            $this->supports = array(
                'products'
            );
        
            // Method with all the options fields
            $this->init_form_fields();
        
            // Load the settings.
            $this->init_settings();
            $this->title = $this->get_option('title');
            $this->description = $this->get_option('description');
            $this->enabled = $this->get_option('enabled');
            $this->testmode = 'yes' === $this->get_option('testmode');
            $this->client_secret = $this->testmode ? $this->get_option('test_client_secret') : $this->get_option('client_secret');
            $this->client_id = $this->testmode ? $this->get_option('test_client_id') : $this->get_option('client_id');
        
            // This action hook saves the settings
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
            add_action( 'woocommerce_api_airtel-money-payment-complete', array( $this, 'webhook' ) );
            add_action('init', 'webhook');
        }

		/**
 		 * Plugin options, we deal with it in Step 3 too
 		 */
          public function init_form_fields(){

            $this->form_fields = array(
                'enabled' => array(
                    'title'       => 'Enable/Disable',
                    'label'       => 'Enable ACL Airtel Money GA Gateway',
                    'type'        => 'checkbox',
                    'description' => '',
                    'default'     => 'no'
               ),
                'title' => array(
                    'title'       => 'Title',
                    'type'        => 'text',
                    'description' => 'This controls the title which the user sees during checkout.',
                    'default'     => 'Airtel Money',
                    'desc_tip'    => true,
               ),
                'description' => array(
                    'title'       => 'Description',
                    'type'        => 'textarea',
                    'description' => 'This controls the description which the user sees during checkout.',
                    'default'     => 'Payez en utilisant Airtel Money (Gabon uniquement).',
               ),
                'testmode' => array(
                    'title'       => 'Test mode',
                    'label'       => 'Enable Test Mode',
                    'type'        => 'checkbox',
                    'description' => 'Place the payment gateway in test mode using test API keys.',
                    'default'     => 'yes',
                    'desc_tip'    => true,
               ),
                'test_client_id' => array(
                    'title'       => 'Test Client Id',
                    'type'        => 'password'
               ),
                'test_client_secret' => array(
                    'title'       => 'Test Client Secret',
                    'type'        => 'password',
               ),
                'client_id' => array(
                    'title'       => 'Live Client Id',
                    'type'        => 'password'
               ),
                'client_secret' => array(
                    'title'       => 'Live Client Secret',
                    'type'        => 'password'
               )
           );
        }

		/*
		 * We're processing the payments here, everything about it is in Step 5
		 */
		public function process_payment($order_id) {
            global $woocommerce;
 
            $order = wc_get_order($order_id);

            // Authentication
            $auth_response = $this->acl_am_auth();
            if (empty($auth_response)) {
                wc_add_notice('Cannot reach Airtel Money Payment Service. '.print_r($auth_response), 'error' );
			    return;
            }

            // Push
            $response = $this->acl_am_push($auth_response['access_token'], $order);

            if (empty($response)) {
                wc_add_notice('Cannot proceed the payment. Please verify your phone number and retry.', 'error' );
			    return;
            }

            if ($order_id =! $response['order_id']){
                wc_add_notice('An error occured during transaction. Please contact the admin at contact@bwelitribe.com', 'error' );
                return;
            }
            
            $order->reduce_order_stock();
            $order->add_order_note('Waiting for customer to enter pin.', true );
            $woocommerce->cart->empty_cart();

            return array(
				'result' => 'success',
				'redirect' => $this->get_return_url($order)
			);
	 	}

		/*
		 * In case you need a webhook, like PayPal IPN etc
		 */
		public function webhook() { 
            $request = json_decode(file_get_contents('php://input'),TRUE);
            
            if (!isset($request['transaction'])){
                $this->logger('info', 'Error updating transaction');
                return;
            }

            $order_id = $request['transaction']['id'];
            $this->logger('info', 'Updating transaction: '.$order_id);

            $message = $request['transaction']['message'];
            $status = ($request['transaction']['status_code'] == 'TS') ? true : false;


            try {
                $order = wc_get_order($order_id);
            } catch (\Exception $ex) {
                $this->logger('error', 'This order does not exists:'.$order_id);
                return;
            }
            
            $order->add_order_note( $message );
            
            if ($status) {
                $order->payment_complete();
	            update_option('webhook_debug', $request);
                $this->logger('info', 'Update done: completed');
                return;
            }
    
            $order->update_status('cancelled');
            $this->logger('info', 'Update done: cancelled');
            $order->add_order_note( $message );
	 	}
        

        private function acl_am_auth() {
            $endpoint = $this->base_uri.$this->payment_uri.$this->auth_uri;
            
            if ($this->testmode == 'yes') {
                $endpoint = $this->base_uri_test.$this->payment_uri.$this->auth_uri;
            }


            $body = array(
                'client_id' => $this->client_id,
                'client_secret' => $this->client_secret,
                'grant_type' => 'client_credentials'
            );

            $args = array(
                'headers' => array(
                    'Content-Type' => 'application/json',
                    'Accept' => '*/*'
                ),
                'body'      => json_encode($body),
                'method'    => 'POST'
            );

            $this->logger('info', 'Starting authentication');
            $response = wp_remote_post($endpoint, $args);
            
            if(!is_wp_error($response)) {
                $this->logger('info', 'Getting authentication');
                $body = json_decode($response['body'], true );

                if (isset($body['token_type']) && isset($body['access_token'])){
                    $this->logger('info', 'Success getting authentication');
                    return array(
                        'token_type' => $body['token_type'],
                        'access_token' => $body['access_token']
                    );
                }
                $this->logger('info', "Fail getting authentication.");
                return array();
            }
            $this->logger('error', "Error getting authentication");
            return array();
        }

        private function acl_am_push($access_token, $order) {
            $endpoint = $this->base_uri.$this->payment_uri;
            if ($this->testmode == 'yes') {
                $endpoint = $this->base_uri_test.$this->payment_uri;
            }

            $order_id = trim(str_replace('#', '', $order->get_order_number()));
            
            $body = array(
                "reference" =>  $order->get_id(),
                "subscriber" =>  array(
                    "country" =>  "GA",
                    "currency" =>  "CFA",
                    "msisdn" => $order->get_billing_phone()
                ),
                "transaction" =>  array(
                  "amount" =>  $order->get_total(),
                  "country" =>  "GA",
                  "currency" =>  "CFA",
                  "id" =>  $order->get_id()
                )
            );

            $args = array(
                'headers' => array(
                    'Authorization' => 'Bearer '.$access_token,
                    'X-Country' => 'GA',
                    'X-Currency' => 'CFA',
                    'Content-Type' => 'application/json',
                ),
                'body'      => json_encode($body),
                'method'    => 'POST'
            );

            $this->logger('info', 'Pushing');
            $response = wp_remote_post($endpoint, $args);
            
            if(!is_wp_error($response)) {
                $body = json_decode($response['body'], true);

                if (isset($body['data']) && isset($body['status'])){
                    $this->logger('info', 'Success push');
                    return array(
                        'order_id' => $body['data']['transaction']['id'],
                        'status' => $body['data']['transaction']['status'],
                        'success' => $body['status']['success']
                    );
                }
                $this->logger('info', "Error pushing");
                $this->logger('error',
                    "######DEBUG#####\n".'Phone: '.$order->get_billing_phone()."\n".'Order: '.$order->get_order_number()."\n".wc_print_r($body, true)
                    );
                return array();
            }
        
            $this->logger('info', "Error response");
            $this->logger('error', "Order Id:".$order->get_id()."\nPhone: ".$order->get_billing_phone());

            return array();
        }

        private function logger($level, $message) {
            $logger = wc_get_logger();

            switch($level) {
                case "info":
                    $logger->info($message, array( 'source' => 'acl-am-info' )); 
                    break;
                case "error":
                    $logger->error($message, array( 'source' => 'acl-am-error' )); 
                    break;
                default: 
                    $logger->debug($message, array( 'source' => 'acl-am-debug' ));
                    break;
            }
        }
 	}
}